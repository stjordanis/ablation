"""
Created on September 16, 2015

@author: Andre Biedenkapp
"""

import unittest
import logging
import os

from ablation.analysis.ablation_analyser import AblationAnalyser

logger = logging.getLogger('TEST-RESULTS')


class TestArguments():
    # -----------------------------------------------------------------------------------------------------------------
    def __init__(self):
        self.epm_model = 'rf'
        self.working_dir = '.'
        self.scenario = './merged/scenario.txt'
        self.logy = True


class Test(unittest.TestCase):
    # -----------------------------------------------------------------------------------------------------------------
    def setUp(self):
        logging.basicConfig(level=logging.DEBUG)
        os.chdir('/home/biedenka/git/ablation/tests/files/spear')
        self.src_dir = os.path.dirname(os.path.dirname(
                                       os.path.abspath(__file__)))

        self.args_ = TestArguments()
        self.aa = AblationAnalyser(self.args_)
        self.aa._gather_data()
        self.aa._train_epm()

        self.target = self.aa.get_optimized_config(os.path.join(self.aa.args_.working_dir, 'traj-run-1.txt'))

        logging.info('\n')
        logging.info('Setup Done')

    """
    def test_origAblation(self):
        source = '"'+"-sp-clause-activity-inc '0.5' -sp-clause-decay '2.0' -sp-clause-del-heur '0'\
                 -sp-clause-inversion '1' -sp-first-restart '100' -sp-learned-clause-sort-heur '0'\
                 -sp-learned-clauses-inc '1.3' -sp-learned-size-factor '0.4' -sp-max-res-lit-inc '1'\
                 -sp-max-res-runs '4' -sp-orig-clause-sort-heur '0' -sp-phase-dec-heur '5'\
                 -sp-rand-phase-dec-freq '0.001' -sp-rand-phase-scaling '1' -sp-rand-var-dec-freq '0.001'\
                 -sp-rand-var-dec-scaling '1' -sp-res-cutoff-cls '8' -sp-res-cutoff-lits '400'\
                 -sp-res-order-heur '0' -sp-resolution '1' -sp-restart-inc '1.5' -sp-update-dec-queue '1'\
                 -sp-use-pure-literal-rule '1' -sp-var-activity-inc '1' -sp-var-dec-heur '0'\
                 -sp-variable-decay '1.4'"+'"'

        tmpTarget = ''
        for i in self.target:
            tmpTarget += '-'+i+" '"+self.target[i]+"' "

        self.aa.ablate('/home/biedenka/git/ablationAnalysis-0.9.2',
                        source=source, target=tmpTarget, algo='ruby spear_wrapper.rb')
    """

    # -----------------------------------------------------------------------------------------------------------------
    def test_init(self):
        attr = vars(self.aa)
        logging.info('\n')
        logging.info('Asserting that everything is initialized')
        for itm in attr.items():
            assert not itm[1] is None,  '{:>16s} is not None: {:>s}'.format(itm[0], str(not(itm[1] is None)))

        logging.info('\nTesting "no need for ablation" (Should display a Logging Error)')
        same = self.aa.ablate_with_epm(self.target, self.target)
        self.assertEqual(same, [])

        source = {'sp-clause-del-heur': '1',
                  'sp-orig-clause-sort-heur': '17',
                  'sp-rand-phase-scaling': '0.3',
                  'sp-var-activity-inc': '0.5',
                  'sp-clause-inversion': '0',
                  'sp-update-dec-queue': '0',
                  'sp-clause-decay': '2.0',
                  'sp-max-res-lit-inc': '4',
                  'sp-res-cutoff-cls': '20',
                  'sp-clause-activity-inc': '0.5',
                  'sp-restart-inc': '1.7',
                  'sp-rand-phase-dec-freq': '0.01',
                  'sp-rand-var-dec-scaling': '0.6',
                  'sp-learned-clauses-inc': '1.4',
                  # 'sp-res-order-heu': '11',
                  'sp-phase-dec-heur': '1',
                  'sp-learned-clause-sort-heur': '9',
                  'sp-variable-decay': '1.1',
                  'sp-rand-var-dec-freq': '0',
                  'sp-first-restart': '25',
                  'sp-learned-size-factor': '0.2',
                  'sp-resolution': '2',
                  'sp-max-res-runs': '16',
                  'sp-res-cutoff-lits': '200',
                  'sp-var-dec-heur': '11',
                  'sp-use-pure-literal-rule': '0'}
        logging.info('\nTesting different source and target config')
        logging.info(self.aa.ablate_with_epm(self.target, source))


# ---------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    unittest.main()
