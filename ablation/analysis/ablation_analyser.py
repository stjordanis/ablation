'''
Created on September 16, 2015

@author: Andre Biedenkapp
'''

from __future__ import print_function

import os
import logging
import glob
import numpy
import sys
import cPickle
import json
import copy
import subprocess
import functools

from scipy import stats

from pysmac.utils.smac_input_readers import read_scenario_file
from pysmac.utils.state_merge import read_sate_run_folder
from pysmac.utils.state_merge import state_merge

from epm.preprocessing.pre_feature import PreprocessingFeatures
from epm.pcs.config_space import ConfigSpace
from epm.preprocessing.fast_imputor_missing_y import FastImputorY as ImputorY
from epm.models import model_dict
from epm.models.external_rfr import ExternalRFR
from epm.experiment_utils.data_handling import build_data
from epm.experiment_utils.data_handling import separate_data_with_bools


class AblationAnalyser(object):
    """
    Analyses the parameter importance of algorithm configurations for given
    instance sets.
    """
    # -----------------------------------------------------------------------------------------------------------------
    def __init__(self, args):
        '''
        Constructor
        '''
        self.logger = logging.getLogger('AAnalyser')
        ch = logging.StreamHandler()
        formatter = logging.Formatter('%(name)s:%(levelname)s: %(message)s')
        ch.setFormatter(formatter)
        ch.setLevel(logging.DEBUG)

        self.def_valid = None
        self.inc_valid = None

        self.logger.addHandler(ch)
        self.args_ = args

        self._scenario = None
        self.scenario_data = None

        self.train_insts = None  # list
        # tuple - first entry is a list of the feature names, second one is a
        # dict with 'instance name' - 'numpy array containing the features'
        # key-value pairs
        self.inst_feats = None
        self.train_inst_feats = None  # dictionary

        self.rr_data = None  # runs and results
        self.ps_data = None  # config callstrings
        self.cs = None  # config_space object
        self.config_list = None  # list of configurations used during SMAC runs (used for epm training)
        self.perf_list = None  # list of performances of configs during SMAC runs (used for epm training)
        self.epm = None  # epm object

        self._setup()

    def _get_data(self, file_):
        with open(file_, 'r') as fh:
            data = fh.read().strip().replace(', ', ',').split('\n')
            if ' ' in data[0]:
                data = map(lambda x: x.split(' ')[0], data)
        return data

    def read_instance_features_file(self, file_):
        data = self._get_data(file_)[1:]
        if data[-1] == '':
            data = data[:-1]

        inst_feat_dict = {}
        for line in data:
            line = line.strip().split(',')
            inst_feat_dict[line[0]] = map(lambda x: float(x.strip()), line[1:])
        self.n_feats_used = len(line[1:])
        return inst_feat_dict

    def read_instances_file(self, file_):
        return self._get_data(file_)

    # -----------------------------------------------------------------------------------------------------------------
    def _setup(self):
        '''
        Reads the necessary files, e.g. scenari-file using spysmac.
        The data are later used for training the epm
        '''
        self._scenario = os.path.split(self.args_.scenario)[-1].split('.')[0]
        # read scenario file
        self.scenario_data = read_scenario_file(self.args_.scenario)
        self.logger.info('Scenario: %s' % self._scenario)
        self.logger.info('Work dir: %s' % str(self.args_.working_dir))

        self.train_insts = self.read_instances_file(
            self.scenario_data["instance_file"])  # returns a list

        if self.scenario_data.get("feature_file"):

            self.inst_feats_dict = self.read_instance_features_file(
                self.scenario_data["feature_file"])

            self.train_inst_feats = dict(
                (inst, self.inst_feats_dict[inst]) for inst in self.train_insts)

    # -----------------------------------------------------------------------------------------------------------------
    def _find_differences(self, source, target):
        """
        Determines which parameters are different in source and target
        """
        if source is None:
            source = self.cs.get_default_config_dict()

        to_walk = []
        self.logger.debug(source)
        self.logger.debug(target)
        for key in source.keys():
            if source[key] != target[key]:
                self.logger.debug('key: {:>16s} s: {:>20s} t: {:>20s}'.format(key, str(source[key]), str(target[key])))
                to_walk.append([key])

        if len(to_walk) == 0:
            self.logger.error('No need for ablation')
        return to_walk

    # -----------------------------------------------------------------------------------------------------------------
    def predict_config_performance_on_instances(self, new_config, inst_list, feat_dict, new=True):
        """
        Returns the prediction for a given configuration on a given list of instances
        """
        mat = self._epm_config_representation([new_config for i in range(len(inst_list))])  # config matrix
        if self.args_.epm_model in ['rf']:
            mat = self.cs.encode(mat)  # one-hot-encoded matrix

        test_X = build_data(mat, inst_list, feat_dict, self.n_feats_used)
        if self.args_.epm_model in ['rf']:
            return self.epm.predict(test_X.astype(numpy.float32))
        if new:
            return self.predict_mv(test_X, logged=False)
        return self.epm.predict_mv(test_X, logged=self.args_.logy)    # return self.predict_mv(test_X, logged=False)

    # -----------------------------------------------------------------------------------------------------------------
    def _is_forbidden(self, new_config):
        skipPrediction = False  # Used for forbidden configurations -> sets prediction
        for forbiddenval in self.cs.forbiddens:  # to infinity so they have no chance to be continued in the next round
            count = 0
            for param_val_pair in forbiddenval:
                if new_config[param_val_pair[0]] in param_val_pair[1]:
                    self.logger.warning('Forbidden values encountered')
                    count += 1
            if count == len(forbiddenval):  # only if all values in a
                                            # forbidden combination are set as specified they are forbidden.
                skipPrediction = True
                break
        return skipPrediction

    # -----------------------------------------------------------------------------------------------------------------
    def _ablation(self, to_walk, is_parent, is_child, source, target, racing=False,
                  racingRoundsBeforeFirstElimination=5, closer_look_at_data=False, new=True):
        if closer_look_at_data:
            num_on_path_during_training_censored = 0
            num_on_path_during_training = 0
            for idx, conf in enumerate(self.config_list):
                not_on_path = False
                for key in conf:
                    if conf[key] not in [source[key], target[key]]:
                        not_on_path = True
                        break
                if not not_on_path:
                    if self.cen_list[idx]:
                        num_on_path_during_training_censored += 1
                    num_on_path_during_training += 1
            self.logger.info('~~~~~~~~~~~~~~~~~~ Training data on path %d / %d' % (num_on_path_during_training_censored,
                                                                                   num_on_path_during_training))
        """
        In every round every remaining parameter gets flipped, to determine which gives the most improvement
        from the source to the target configuration
        """
        assert len(source) == len(target)
        self.logger.info('{:>30s}: Source / Target (on path, child, parent)'.format('Parameter'))
        for i in source:
            onPath = ' '
            child = '_'
            parent = '_'
            if [i] in to_walk:
                onPath = 'X'
            if i in is_parent:
                parent = 'P'
            if i in is_child:
                child = 'C'
            self.logger.info('{:>30s}: {:>6s} / {:>6s} ({:>1s}, {:>1s}, {:>1s})'.format(i, str(source[i]), str(target[i]),
                             onPath, child, parent))

        if not racing:
            return self.walk_path(to_walk, is_parent, is_child, source, target, new=new)
        return self.walk_path_with_racing(to_walk, is_parent, is_child, source, target, racing,
                                          racingRoundsBeforeFirstElimination, new=new)

    # -----------------------------------------------------------------------------------------------------------------
    def walk_path(self, to_walk, is_parent, is_child, source, target, new=True):
        if new:
            self.logger.debug('Setting args.logy to False')
            self.args_.logy = False
        configs = list()
        _round = 1
        orig_source = copy.deepcopy(source)
        inst_list = self.train_insts
        source_performance = self.predict_config_performance_on_instances(source, inst_list,
                                                                          self.inst_feats_dict, new=new)
        var = source_performance[1]
        source_performance = source_performance[0]
        configs.append(('def', source_performance))

        if self.args_.logy:
            self.logger.info('Predicted default performance %f, %f' % (numpy.mean(numpy.power(10, source_performance)),
                                                                       numpy.mean(var) + numpy.var(source_performance)))
        else:
            self.logger.info('Predicted default performance %f, %f' % (numpy.mean(source_performance),
                                                                       numpy.mean(var))) #+ numpy.var(source_performance)))
        diff = len(to_walk)

        self.logger.info('#'*120)
        self.logger.info('Beginning Ablation')
        while len(to_walk) > 0:
            mean_prediction = []
            vars_ = []
            for idx, param_tuple in enumerate(to_walk):
                new_config = copy.deepcopy(source)
                for param in param_tuple:
                    new_config[param] = target[param]
                skipprediction = self._is_forbidden(new_config)
                if skipprediction:
                    continue
                else:  # predict the performance
                    pred_var = self.predict_config_performance_on_instances(new_config, inst_list,
                                                                            self.inst_feats_dict, new=new)
                    pred = pred_var[0]
                    var = pred_var[1]
                    vars_.append(numpy.mean(var)) # + numpy.var(pred))
                if self.args_.logy:
                    pow10 = numpy.power(10, pred)
                    mean_prediction.append(numpy.mean(pow10))
                else:
                    mean_prediction.append(numpy.mean(pred))
            self.logger.info('#'*120)
            tmp_pred_val = 0
            tmp_at = 0
            for i, j in zip(mean_prediction, to_walk):
                tmp_pred_val = i
                self.logger.info('{:>60s}: {:>f} {:>f}'.format(j, tmp_pred_val, vars_[tmp_at]))
                tmp_at += 1

            best_at = numpy.argmin(mean_prediction)
            best = mean_prediction[best_at]
            best_param = to_walk[best_at]

            self.logger.debug('Distance  s->ns: {:>2} ns->t: {:>2}'.format(
                self._dict_dist(orig_source, new_config), self._dict_dist(new_config, target)))

            configs.append((best_param, best))
            to_walk.remove(best_param)

            tmp_pred_val = best
            str_bp = ''
            if len(best_param) > 1:
                str_bp = str(best_param)
                source_str = str(map(lambda x: str(orig_source[x]), best_param))
                target_str = str(map(lambda x: str(target[x]), best_param))
            else:
                str_bp = str(best_param[0])
                source_str = orig_source[best_param[0]]
                target_str = target[best_param[0]]
            self.logger.info('Round {:>3d} Flipped {:>30s} {:>16s} to achive predicted performance {:>f}'.format(_round,
                             str_bp, '%4s to -> %4s ' % (
                                source_str, target_str), tmp_pred_val))

            to_walk = self.handle_conditionals(best_param, is_parent, is_child, to_walk, source, target)

            new_config = copy.deepcopy(source)
            for best_p in best_param:
                new_config[best_p] = target[best_p]

            source = copy.deepcopy(new_config)
            _round += 1
        self.logger.info('#'*120)
        self.logger.info('Finished Ablation')
        self.logger.info('#'*120)
        return configs

    def from_j_to_i(self, expectation):
        """
        helper method
        :type expectation: list
        """
        res = 0
        for a in range(1, len(expectation)):
            for b in range(a-1):
                res += expectation[a]*expectation[b]
        return res

    # -----------------------------------------------------------------------------------------------------------------
    def walk_path_with_racing(self, to_walk, is_parent, is_child, source, target, racing=False,
                              racingRoundsBeforeFirstElimination=5, new=True):
        configs = list()
        _round = 1
        orig_source = copy.deepcopy(source)
        inst_list = self.train_insts
        pred_var = self.predict_config_performance_on_instances(source, inst_list, self.train_inst_feats, new=new)
        pred = pred_var[0]
        var = numpy.mean(pred_var[1])
        source_performance = numpy.mean(pred)
        configs.append(('def', source_performance))

        if self.args_.logy:
            self.logger.info('Predicted default performance %f' % (numpy.power(10, source_performance)))
        else:
            self.logger.info('Predicted default performance %f' % (source_performance))
        diff = len(to_walk)

        self.logger.info('#'*120)
        self.logger.info('Beginning Ablation')
        while len(to_walk) > 0:
            rank_matrix = []
            prediction_matrix = []
            drop = []
            best_idx = None
            if racing:
                to_walk_duplicate = copy.deepcopy(to_walk)
            for i in range(len(inst_list)):
                prediction_list = []
                for idx, param_tuple in enumerate(to_walk):
                    if racing and param not in to_walk_duplicate:
                        continue
                    # Flipp the parameter that gets tested
                    new_config = copy.deepcopy(source)
                    for param in param_tuple:
                        new_config[param] = target[param]
                    skipprediction = self._is_forbidden(new_config)
                    if skipprediction:
                        continue
                    else:  # predict the performance
                        pred_var = self.predict_config_performance_on_instances(new_config, [inst_list[i]],
                                                                            self.train_inst_feats, new=new)
                        pred = pred_var[0][0]
                        var = pred_var[1][0]
                    prediction_list.append(pred)
                prediction_matrix.append(prediction_list)
                if racing and len(to_walk) > 1:  # perform friedman test and, if H0 was not rejected ...
                    rank_matrix.append(list(stats.rankdata(prediction_list)))
                    if i >= racingRoundsBeforeFirstElimination:     # ... perform pairwise comparisons to
                                                                    # determine which parameters can be dropped
                        drop = self.friedman(rank_matrix, len(rank_matrix), len(rank_matrix[0]))
                        drop = sorted(drop, reverse=True)

                        for d in drop:  # Now drop them
                            if d in drop:
                                continue
                            prediction_matrix = list(numpy.delete(prediction_matrix, d, axis=1))
                            rank_matrix = list(numpy.delete(rank_matrix, d, axis=1))
                            to_walk_duplicate = list(numpy.delete(to_walk_duplicate, d))

                        if len(to_walk_duplicate) == 1:  # if we only have one config left, return it as best config
                            best_idx = to_walk_duplicate[0]
                            break
            mean_prediction = numpy.mean(prediction_matrix, axis=0)
            self.logger.info('#'*120)
            tmp_pred_val = 0
            for i, j in zip(mean_prediction, to_walk):
                tmp_pred_val = i
                if self.args_.logy:
                    tmp_pred_val = numpy.power(10, tmp_pred_val)
                self.logger.info('{:>60s}: {:>f}'.format(j, tmp_pred_val))

            # Determine the best parameter
            if best_idx is not None:
                best = mean_prediction[best_idx]
                best_param = to_walk[best_idx]
            else:
                best = min(mean_prediction)
                best_param = to_walk[numpy.argmin(mean_prediction)]

            self.logger.debug('Distance  s->ns: {:>2} ns->t: {:>2}'.format(
                self._dict_dist(orig_source, new_config), self._dict_dist(new_config, target)))

            configs.append((best_param, best))
            to_walk.remove(best_param)

            tmp_pred_val = best
            if self.args_.logy:
                tmp_pred_val = numpy.power(10, tmp_pred_val)
            str_bp = ''
            if len(best_param) > 1:
                str_bp = str(best_param)
                source_str = str(map(lambda x: str(orig_source[x]), best_param))
                target_str = str(map(lambda x: str(target[x]), best_param))
            else:
                str_bp = str(best_param[0])
                source_str = orig_source[best_param[0]]
                target_str = target[best_param[0]]
            self.logger.info('Round {:>3d} Flipped {:>30s} {:>16s} to achive predicted performance {:>f}'.format(_round,
                             str_bp, '%4s to -> %4s ' % (
                                source_str, target_str), tmp_pred_val))

            to_walk = self.handle_conditionals(best_param, is_parent, is_child, to_walk, source, target)

            new_config = copy.deepcopy(source)
            for best_p in best_param:
                new_config[best_p] = target[best_p]

            source = copy.deepcopy(new_config)
            _round += 1
        self.logger.info('#'*120)
        self.logger.info('Finished Ablation')
        self.logger.info('#'*120)
        return configs

    # -----------------------------------------------------------------------------------------------------------------
    def handle_conditionals(self, best_param, is_parent, is_child, to_walk, source, target):
        # Check if we flipped a parent to a value that changes it's children
        if len(best_param) == 1 and best_param[0] in is_parent:
            delete_this = filter(lambda x: len(x) > 1 and x[1] == best_param[-1], to_walk)
            for deletable in delete_this:
                to_walk.remove(deletable)
            for child in is_child:
                if child in to_walk and best_param in is_child[child][0]:
                    allowed_values = is_child[child][1]
                    if source[best_param] in allowed_values and target[best_param] not in allowed_values:
                        self.logger.info('Child %s is deactivated' % child)
                        to_walk.remove(child)
                    elif source[best_param] not in allowed_values and target[best_param] in allowed_values:
                        self.logger.info('Child %s is activated' % child)
                        to_walk.append(child)
                    else:
                        self.logger.info('No changes for child %s' % child)
        if len(best_param) == 1 and best_param[0] in is_child:
            delete_this = filter(lambda x: len(x) > 1 and x[0] == best_param[0], to_walk)
            for deletable in delete_this:
                to_walk.remove(deletable)
        if len(best_param) > 1:
            paird_flips = filter(lambda x: len(x) > 1, to_walk)
            for bp in best_param:
                if [bp] in to_walk:
                    to_walk.remove([bp])
                for pair in paird_flips:
                    if bp in pair and pair in to_walk:
                        to_walk.remove(pair)
        return to_walk

    # -----------------------------------------------------------------------------------------------------------------
    def friedman(self, ranks, k, n, alpha=0.05):
        """
        Implementation of the formulas given in the f-race paper.
        This uses the ranks of the predictions to first perform the friedman-test.
        If the test passes, then pairwise comparisons are made, to determine if it is
        possible to "skip" a configuration for further evaluation in the current racing round
        """
        df = n - 1  # degrees of freedom
        column_rank = numpy.sum(ranks, axis=0)
        reusable = (k * n * (n + 1) ** 2) / 4.
        numerator = (n - 1) * numpy.sum(map(lambda x: (x - ((k * (n + 1)) / 2.)) ** 2, column_rank))
        denominator = numpy.sum(numpy.power(ranks, 2)) - reusable
        fried_val = numerator / denominator
        p_val = stats.chi2.sf(fried_val, df)

        drop = []
        if p_val < alpha:
            root_denominator = (k - 1) * (n - 1)
            root_numerator = 2. * k * (1. - (fried_val / (k * (n - 1)))) * (numpy.sum(numpy.power(ranks, 2)) - reusable)

            denominator = numpy.sqrt(root_numerator / root_denominator)
            for i in range(len(column_rank) - 1):
                if i in drop:
                    continue
                for j in range(i + 1, len(column_rank)):
                    if j in drop:
                        continue
                    numerator = numpy.abs(column_rank[i] - column_rank[j])
                    test_val = numerator / denominator

                    if test_val > stats.t.interval(1 - (alpha / 2), df)[1]:
                        if column_rank[i] - column_rank[j] < 0:
                            drop.append(i)
                        else:
                            drop.append(j)

        return drop  # the indices of the parameters that can be skipped

    # -----------------------------------------------------------------------------------------------------------------
    def _determine_parents(self, to_walk, source, target):
        """
        Determines whether or not a parameter is a parent.
        If a parameter is a parent, it's children will also be flipped
        """
        is_child = {}
        is_parent = {}
        for i in self.cs.conditions:
            is_child[i.cond] = (i.head, i.values)
            if i.head not in is_parent:
                is_parent[i.head] = []
            is_parent[i.head].append(i.cond)

        tmp = copy.deepcopy(to_walk)
        fulfills_condition = []
        for j, i in enumerate(tmp):
            if i[0] in is_child and (source[is_child[i[0]][0]] in is_child[i[0]][1] or target[is_child[i[0]][0]] in is_child[i[0]][1]):
                fulfills_condition.append(i[0])  # ... flipped.
            elif i[0] in is_child:
                del to_walk[to_walk.index(i)]  # ... parents don't fulfill the conditions, because it would not matter for the ...
        for child in fulfills_condition:  # only if the child is not active to begin with
            if source[is_child[child][0]] not in is_child[child][1]:
                to_walk.append([child, is_child[child][0]])  # paird flipps
        return to_walk, is_parent, is_child  # ... final configuration.

    # -----------------------------------------------------------------------------------------------------------------
    def ablate_with_epm(self, target, source=None, racing=True, closer_look_at_data=False, new=True):
        """
        Implementation of ablation using EPM(s)

        :source: dictionary with source configuration
        :target: dictionary with target configuration
        """
        # Determine parameters to flipp
        to_walk, is_parent, is_child = self._determine_parents(self._find_differences(source, target), source, target)
        return self._ablation(to_walk, is_parent, is_child, source, target,
                              racing, closer_look_at_data=closer_look_at_data, new=new)

    # -----------------------------------------------------------------------------------------------------------------
    def _dict_dist(self, a, b):
        """
        Helper Method for showing the progress in the logs
        """
        res = 0
        for k in a.keys():
            if a[k] != b[k]:
                res = res + 1
        return res

    # -----------------------------------------------------------------------------------------------------------------
    def get_default_config(self, pcs_file):
        """
        Reads the default configuration from a given PCS files
        """
        if self.cs == None:
            pcs_file = self.scenario_data["paramfile"]
            self.cs = ConfigSpace(pcs_file)
        values = self.cs.parameters.itervalues()
        return dict((p.name,p.default) for p in values)

    # -----------------------------------------------------------------------------------------------------------------
    def generateOtpionFile(self, racing=False, source='DEFAULT', target=None, algo='', writeTo='.', maxRounds=-1):
        """
        Creates the Option File needed for the original version of ablation
        """
        scenarioFile = os.path.join(
            self.args_.working_dir, 'merged', 'scenario.txt')
        optimized = target

        race = 'false'
        maxRoundsString = 'maximumAblationRounds'
        if racing:
            race = 'true'
            maxRoundsString = 'maximumRacingRounds'

        # Dictionary containing the necessary entries of the options file
        neededEntries = {
            'algo': algo,
            'execdir': './',
            'experimentDir': './',
            'deterministic': '0',
            'run_obj': 'runtime',
            'overall_obj': 'mean',
            'cutoff_time': '20',
            'cutoff_length': 'max',
            'seed': '1234',
            'cli-concurrent-execution': 'true',
            'cli-cores': '1',
            'useRacing': race,
            'paramfile': os.path.sep.join(os.path.abspath(
                self._scenario).split(os.path.sep)[:-1] + ['param.pcs']),  # (pcs file in smac)
            'instance_file': './',
            'test_instance_file': './',
            'sourceConfiguration': source,
            'targetConfiguration': optimized}
        order = ['algo', 'execdir', 'experimentDir', 'deterministic', 'run_obj', 'overall_obj',
                 'cutoff_time', 'cutoff_length', 'seed', 'cli-concurrent-execution', 'cli-cores',
                 'useRacing', 'paramfile', 'instance_file', 'test_instance_file',
                 'sourceConfiguration', 'targetConfiguration']

        if maxRounds > 0:
            neededEntries[maxRoundsString] = str(maxRounds)
            order.append(maxRoundsString)

        cleaned = []
        idx = {}
        lineidx = 0
        with open(scenarioFile, 'r') as sF:
            for line in sF:
                clean = line.strip('\n').split(' ')
                key = clean[0].strip(' ')
                idx[key] = lineidx
                cleaned.append(' '.join(map(lambda x: x.strip(' ').strip('\n'), clean[1:])))
                lineidx += 1

        result = []
        for entry in order:
            try:
                if entry == 'algo':
                    result.append(' = '.join((entry, algo)))
                elif entry == 'deterministic':
                    if cleaned[idx['algo-deterministic']].lower() == 'false':
                        result.append(' = '.join((entry, '0')))
                    else:
                        result.append(''.join((entry, '1')))
                elif entry == 'experimentDir':
                    result.append(' = '.join((entry, '.')))
                elif entry == 'instance_file':
                    result.append(' '.join((entry, os.path.sep.join(
                        self.args_.working_dir.split(os.path.sep)[:-2] + [cleaned[idx['instance_file']][2:]]))))
                elif entry == 'test_instance_file':
                    result.append(' '.join((entry, os.path.sep.join(
                        self.args_.working_dir.split(os.path.sep)[:-2] + [cleaned[idx['instance_file']].replace(
                            'training', 'test')[2:]]))))
                elif entry == 'execdir':
                    result.append(' = '.join((entry, cleaned[idx['algo-exec-dir']][2:])))
                elif entry == 'paramfile':
                    result.append(' = '.join((entry, cleaned[idx['pcs-file']])))
                elif entry == 'run_obj':
                    result.append(' = '.join((entry, cleaned[idx['run-obj']].lower())))
                elif entry == 'overall_obj':
                    result.append(' '.join((entry, cleaned[idx[entry]].lower())))
                else:
                    result.append(' '.join((entry, cleaned[idx[entry]])))
            except KeyError:
                result.append(' = '.join((entry, neededEntries[entry])))

        writeTo = os.path.join(os.path.realpath(writeTo), 'optionFile.txt')  # os.path.abspath(writeTo)

        result = '\n'.join(result)
        self.logger.info(writeTo)

        with open(writeTo, 'w') as optFile:
            optFile.write(result)

        return writeTo

    # -----------------------------------------------------------------------------------------------------------------
    def ablate(self, jarPath, racing=False, source='DEFAULT', target=None, algo='',
               working_dir='.', maxRounds=-1, oof=False):
        """
        Wrapper to call the original java implementation of Ablation.

        :jarPath: Path to the jar file
        :racing:  Bool. Determines if the racing variant is to be used or not
        :source:  String. The source configuration
        :target:  String. The target ocnfiguration
        :algo:    String. The algorithm to use
        """
        self.logger.info('Starting normal ablation')
        optFile = self.generateOtpionFile(racing, source, target, algo, writeTo=working_dir, maxRounds=maxRounds)
        if not oof:
            ablationstatus = subprocess.call([''.join((jarPath, '/ablationAnalysis')), '--optionFile',
                                              optFile], shell=False)
            self.logger.info('done')
            assert ablationstatus == 0, 'Ablation failed. Check the log folder to find out why'
        else:
            self.logger.info('Created optFile')

    # -----------------------------------------------------------------------------------------------------------------
    def _gather_data(self):
        '''
            gather performance data
        '''
        state_runs = glob.glob(os.path.join(
            self.args_.working_dir, "*state-run*"))  # use old and new data

        merged_state = os.path.join(
            self.args_.working_dir, "merged")

        if not os.path.exists(merged_state):
            state_merge(state_run_directory_list=state_runs,
                        destination=merged_state)

        if self.train_inst_feats is None:
            self.ps_data, self.train_insts, self.train_inst_feats, self.rr_data =\
                read_sate_run_folder(directory=merged_state)
        else:
            self.ps_data, _, tmp, self.rr_data =\
                read_sate_run_folder(directory=merged_state)  # configs, instance-names, inst-feats, runs_and_results
            del tmp

    # -----------------------------------------------------------------------------------------------------------------
    def _epm_config_representation(self, config_list):
        """
        Convert the configuration list into the epm matrix representation
        """
        config_matrix = numpy.zeros((len(config_list), self.cs._n_params))
        for indx, config in enumerate(config_list):
            config_vec = self.cs.convert_param_dict(config)  # TODO don't normalize the config
            imputed_vec = self.cs.impute_non_active(
                config_vec, value="def")  # def imputation
            config_matrix[indx] = imputed_vec
        return config_matrix

    def _drop_censored(self, cen_list, config_list, perf_list, inst_list):
        usable = []
        for didx, dpoint in enumerate(cen_list):
            if not dpoint:
                usable.append(didx)
        config_list = numpy.array(config_list)[usable]
        perf_list = numpy.array(perf_list)[usable]
        inst_list = numpy.array(inst_list)[usable]
        cen_list = numpy.array(cen_list)[usable]
        return config_list, perf_list, inst_list, cen_list

    # -----------------------------------------------------------------------------------------------------------------
    def _train_epm(self, save='model.pkl', save_meta=True, par=2, include_timeouts=True, impute_=True, nsamples=None):
        '''
            transform data to EPM style and train an EPM
        '''

        self.logger.info("### Start training EPM")

        config_list = []
        perf_list = []
        inst_list = []
        cen_list = []

        for data in self.rr_data:
            inst_list.append(self.train_insts[int(data[1]) - 1])

            perf_list.append(data[2])
            if include_timeouts:
                if data[8] == 0:
                    cen = True
                else:
                    cen = False
                cen_list.append(cen)
            else:
                if data[3] == 0:
                    cen = False
                else:
                    cen = True
                cen_list.append(cen)
            config_list.append(self.ps_data[int(data[0]) - 1])  # for each run find the corresponding configuration

        if not impute_:
            config_list, perf_list, inst_list, cen_list =\
                self._drop_censored(cen_list, config_list, perf_list, inst_list)

        if nsamples is not None and nsamples < len(perf_list):
            config_list, perf_list, inst_list, cen_list = self.downsample(nsamples, config_list,
                                                                          perf_list, inst_list, cen_list)

        if self.inc_valid:  # (inc_conf_list, inc_insts_, inc_cen_, inc_perf_)
            inc_config_list, inc_inst_list, inc_cen_list, inc_perf_list = self.inc_valid
            if not impute_:
                inc_config_list, inc_perf_list, inc_inst_list, inc_cen_list = \
                    self._drop_censored(inc_cen_list, inc_config_list, inc_perf_list, inc_inst_list)
            config_list = numpy.append(config_list, inc_config_list)
            inst_list = numpy.append(inst_list, inc_inst_list)
            cen_list = numpy.append(cen_list, inc_cen_list)
            perf_list = numpy.append(perf_list, inc_perf_list)

        if self.def_valid:  # (def_conf_list, def_insts_, def_cen_, def_perf_)
            def_config_list, def_inst_list, def_cen_list, def_perf_list = self.def_valid
            if not impute_:
                def_config_list, def_perf_list, def_inst_list, def_cen_list = \
                    self._drop_censored(def_cen_list, def_config_list, def_perf_list, def_inst_list)
            config_list = numpy.append(config_list, def_config_list)
            inst_list = numpy.append(inst_list, def_inst_list)
            cen_list = numpy.append(cen_list, def_cen_list)
            perf_list = numpy.append(perf_list, def_perf_list)

        self.config_list = config_list
        self.perf_list = perf_list
        self.cen_list = cen_list

        self.logger.info("Number of training data: %d" % (len(perf_list)))
        self.logger.debug("Configs: %s" % len(config_list))
        self.logger.debug("Perfs: %s" % len(perf_list))
        self.logger.debug("Insts: %s" % len(inst_list))
        self.logger.debug("Censored: %s" % len(cen_list))

        # normalize features
        self.logger.info("### Normalize features")
        fpre = PreprocessingFeatures(inst_feats_dict=self.inst_feats_dict)
        self.inst_feats_dict = fpre.normalization(inst_feats_dict=self.inst_feats_dict)

        # Convert training data to matrix, impute nonactive params and
        # normalize
        self.logger.info(
            "### Convert configurations into internal representation")
        pcs_file = self.scenario_data["paramfile"]
        self.cs = ConfigSpace(pcs_file)

        config_matrix = self._epm_config_representation(config_list)

        if self.args_.epm_model == "rf":
            # one-hot encoding
            encoded_matrix = self.cs.encode(X=config_matrix)
        else:
            encoded_matrix = config_matrix

        self.logger.info("Number of training data points: %d" % (encoded_matrix.shape[0]))

        # convert feature dictionary into feature matrix
        feat_matrix = []
        for inst_ in inst_list:
            feat_matrix.append(self.inst_feats_dict[inst_])
        feat_matrix = numpy.array(feat_matrix)

        name, model, boot_model = model_dict.model_dict[self.args_.epm_model]

        cutoff = sys.maxint
        threshold = sys.maxint
        cut_tmp = self.scenario_data.get('cutoff_time')
        try:
            cutoff = int(cut_tmp)
            threshold = par * int(cut_tmp)
            self.cutoff = cutoff
            self.threshold = threshold
        except ValueError:
            pass

        if self.args_.logy:
            threshold = numpy.log10(par * cutoff)
            cutoff = numpy.log10(cutoff)

        if name == 'rf':
            self.logger.info("Use %s as EPM model" % (name))
        elif name == 'rfr':
            self.logger.info("Use ML4AADrf as EPM model")
            boot_model = functools.partial(boot_model.func, cs=self.cs,
                                           n_feats=self.n_feats_used,
                                           **boot_model.keywords)

            model = functools.partial(model.func, cs=self.cs, n_feats=self.n_feats_used,
                                      cutoff=cutoff,
                                      threshold=threshold,
                                      prediction_threshold=0, **model.keywords)
        elif name in ['rfrm', 'mrfrm']:
            self.logger.info("Use ML4AADrf - different hpyers as EPM model")
            boot_model = functools.partial(boot_model.func, cs=self.cs,
                                           n_feats=self.n_feats_used,
                                           **boot_model.keywords)

            model = functools.partial(model.func, cs=self.cs, n_feats=self.n_feats_used,
                                      cutoff=cutoff,
                                      threshold=threshold,
                                      prediction_threshold=0, **model.keywords)
        else:
            self.logger.error("Model %s not implemented" % (self.args_.epm_model))
            sys.exit(3)

        if self.args_.logy:
            self.logger.info('Using logscale')
            if min(perf_list) < 0:
                perf_list += min(perf_list)
            perf_list = numpy.log10(perf_list)

        train_cen_encoded_matrix, train_cen_inst_list, train_cen_y, train_uncen_encoded_matrix,\
            train_uncen_inst_list, train_uncen_y = \
            separate_data_with_bools(data_matrix=encoded_matrix,
                                     inst_list=inst_list,
                                     perf_list=perf_list,
                                     succ_list=cen_list)

        train_uncen_X = build_data(data_matrix=train_uncen_encoded_matrix,
                                   inst_list=train_uncen_inst_list,
                                   inst_feat_dict=self.inst_feats_dict,
                                   n_feats=feat_matrix.shape[1])

        # Impute data for training folds
        if impute_ and (True in cen_list):
            self.logger.info("### Impute missing data")
            imputor = ImputorY(debug=False)

            train_cen_X = build_data(data_matrix=train_cen_encoded_matrix,
                                     inst_list=train_cen_inst_list,
                                     inst_feat_dict=self.inst_feats_dict,
                                     n_feats=feat_matrix.shape[1])

            train_imp_y = imputor.raw_impute_arrays(X_uncen=numpy.array(train_uncen_X).astype(numpy.float32),
                                                    y_uncen=numpy.array(train_uncen_y).astype(numpy.float32),
                                                    X_cen=numpy.array(train_cen_X).astype(numpy.float32),
                                                    y_cen=numpy.array(train_cen_y).astype(numpy.float32),
                                                    model=boot_model,
                                                    par=threshold/float(cutoff),
                                                    cutoff=cutoff,
                                                    log=False, change_threshold=0.01,
                                                    store_info=True)

            train_X = numpy.vstack((train_uncen_X, train_cen_X))
            train_y = numpy.concatenate((train_uncen_y, train_imp_y), axis=0)
        else:
            self.logger.info("No imputation")
            train_X = train_uncen_X
            train_y = numpy.array(train_uncen_y)

        self.logger.info("### Start training EPM")
        self.epm = model()
        self.epm.fit(numpy.array(train_X).astype(numpy.float32), numpy.array(train_y).astype(numpy.float32))

        # rrf does not support pickle right now
        if save:  # and self.args_.epm_model not in ["rfr"]:
            with open(save, "w") as fp:
                cPickle.dump(self.epm, fp)
        with open("meta_cs.pkl", "w") as fp:
            cPickle.dump(self.cs, fp)

        if save_meta:
            tmp_args = self.args_
            tmp_args.logger = None
            meta_data = {"saved_model": save,
                         "pcs": "meta_cs.pkl",
                         "args": vars(tmp_args)
                         }

            with open("meta_model.json", "w") as fp:
                json.dump(meta_data, fp, indent=2)

        self.logger.info('### Done training EPM')
        self.logger.info('##################################################################################')
        self.logger.info('Out of bag error: %f' % self.epm.out_of_bag_error())
        self.logger.info('##################################################################################')
        return self.epm

    # -----------------------------------------------------------------------------------------------------------------
    def downsample(self, nsamples, config_list, perf_list, inst_list, cen_list):
        rs = numpy.random.RandomState(nsamples)
        self.logger.info("Downsampling from %d to %d" % (len(perf_list), nsamples))
        idx = list(range(len(perf_list)))
        rs.shuffle(idx)
        idx = idx[:nsamples]
        config_list = [config_list[i] for i in idx]
        inst_list = [inst_list[i] for i in idx]
        perf_list = [perf_list[i] for i in idx]
        cen_list = [cen_list[i] for i in idx]

        return config_list, perf_list, inst_list, cen_list

    # -----------------------------------------------------------------------------------------------------------------
    def validate_path(self, path, target, source, test_file, save_file, var_file, new=True):
        fpre = PreprocessingFeatures(inst_feats_dict=self.inst_feats_dict)
        inst_feats_dict = fpre.normalization(inst_feats_dict=self.inst_feats_dict)

        new_config = copy.deepcopy(source)
        self.logger.debug('Reading test instances')
        # test_instance_file
        with open(test_file, 'r') as tF:
            instances = tF.read().strip().split('\n')
            if ' ' in instances[0]:
                instances = map(lambda x: x.split(' ')[0], instances)
            if instances[-1] == '':
                instances = instances[:-1]
            test_insts = map(lambda x: x.strip(' '), instances)
        self.logger.debug('%d test instances' % len(test_insts))

        formt_str_ = '{:>20s}{:>50s}{:>20s}{:>20s}{:>30s}\n'
        result_str = 'Ablation analysis validation complete.\n{:140s}\n'.format('-'*140)+formt_str_.format(
            'Round', 'Flipped Parameter', 'Source value', 'Target value', 'Validation result')+'-'*140+'\n'
        strings = [result_str]
        var_strings = [result_str]
        print(strings[-1], end="")
        prediction = self.predict_config_performance_on_instances(
                                source, test_insts, inst_feats_dict, new=new)
        var = prediction[1]
        prediction = prediction[0]
        var = numpy.mean(var)  # + numpy.var(prediction)
        if self.args_.logy:
            prediction = numpy.mean(numpy.power(10, prediction))
        else:
            prediction = numpy.mean(prediction)
        strings.append(formt_str_.format(str(0), '-source-', 'N/A', 'N/A', '%3.5f' % prediction))
        var_strings.append(formt_str_.format(str(0), '-source-', 'N/A', 'N/A', '%3.5f' % var))

        print(strings[-1], end="")
        for index, parameterSet in enumerate(path):
            for parameter in parameterSet:
                new_config[parameter] = target[parameter]
            prediction = self.predict_config_performance_on_instances(
                                    new_config, test_insts, inst_feats_dict, new=new)
            var = prediction[1]
            prediction = prediction[0]
            var = numpy.mean(var)  # + numpy.var(prediction)
            if self.args_.logy:
                prediction = numpy.mean(numpy.power(10, prediction))
            else:
                prediction = numpy.mean(prediction)
            strings.append(formt_str_.format(str(index + 1), ', '.join(parameterSet),
                           ', '.join(map(lambda x: str(source[x]), parameterSet)),
                           ', '.join(map(lambda x: str(target[x]), parameterSet)),
                           '%3.5f' % prediction))
            var_strings.append(formt_str_.format(str(index + 1), ', '.join(parameterSet),
                               ', '.join(map(lambda x: str(source[x]), parameterSet)),
                               ', '.join(map(lambda x: str(target[x]), parameterSet)),
                               '%3.5f' % var))
            print(strings[-1], end="")
        strings.append(formt_str_.format(str(index + 2), '-target-', 'N/A', 'N/A', '%3.5f' % prediction))
        var_strings.append(formt_str_.format(str(index + 2), '-target-', 'N/A', 'N/A', '%3.5f' % var))
        print(strings[-1], end="")
        strings.append('-'*140)
        var_strings.append('-'*140)
        print(strings[-1])

        save_file.write(''.join(strings))
        var_file.write(''.join(var_strings))

    # -----------------------------------------------------------------------------------------------------------------
    def predict_mean_var(self, X):
        """
        returns mean/sqrt(var) prediction
        :param x: numpy vector/matrix, required, features
        :return: mean, variance
        """
        if self.epm.model is None:

            raise ValueError("Model is not yet trained")

        threshold = 10 ** -10
        mean_prediction_over_instances = []
        mean_variance_over_instances = []
        for x in X:
            tmpx = numpy.array(list(map(lambda x_: numpy.power(10, x_), self.epm.model.all_leaf_values(x))))
            mean_prediction_over_instances.append(list(map(lambda x_: numpy.mean(x_), tmpx)))
            mean_variance_over_instances.append(list(map(lambda x_: numpy.var(x_), tmpx)))

        tree_mean_prediction_over_instances = numpy.mean(mean_prediction_over_instances, axis=0)
        tree_var_prediction_over_instances = numpy.mean(mean_variance_over_instances, axis=0)

        mean = numpy.mean(tree_mean_prediction_over_instances)
        var = numpy.mean(tree_var_prediction_over_instances) + numpy.var(tree_mean_prediction_over_instances)
        mean = numpy.array([mean], dtype=self.epm._dtype)
        var = numpy.array([var], dtype=self.epm._dtype)
        var[var < threshold] = threshold
        var[numpy.isnan(var)] = threshold
        return mean, var

# -----------------------------------------------------------------------------------------------------------------
    def predict_mv(self, X, cutoff=None, threshold=None, prediction_threshold=None,
                   logged=False):
        """
        exists to be compatible to sklearn estimators
        :param X: numpy vector/matrix, required
               input features
        :param cutoff : float, optional
               cutoff value from ACLib scenario.
               If None, initial cutoff will be used
        :param threshold : float, optional
               there will be no return value higher than this, e.g. cutoff*par
               If None, initial cutoff will be used
        :param prediction_threshold: float, optional
               is less mass then this > cutoff, return mean prediction
               If None, initial cutoff will be used
        :param logged : bool, optional
               if trained on log10-data, method will use war/unwarp to predict
        :return: prediction
        """
        if cutoff is None:
            tmp_cutoff = self.cutoff
        else:
            tmp_cutoff = cutoff

        if threshold is None:
            tmp_threshold = self.threshold
        else:
            tmp_threshold = threshold

        if prediction_threshold is None:
            tmp_pt = self.epm.prediction_threshold
        else:
            tmp_pt = prediction_threshold

        mean_var = self.predict_mean_var(X=X)
        assert logged == False

        pred = numpy.zeros(shape=mean_var[0].shape, dtype=self.epm._dtype)
        var = numpy.zeros(shape=mean_var[0].shape, dtype=self.epm._dtype)

        for p in range(pred.shape[0]):
            var[p] = mean_var[1][p]
            if mean_var[0][p] > tmp_cutoff:
                # mean prediction is already higher than cutoff
                self.logger.critical("Predicted %g which is higher than cutoff"
                                     " %s" % (mean_var[0][p], tmp_cutoff))
                # pred[p] = tmp_threshold
                # continue

            # Calc cdf from -inf to cutoff
            cdf = stats.norm.cdf(x=tmp_cutoff, loc=mean_var[0][p],
                                       scale=numpy.sqrt(mean_var[1][p]))

            # Probability mass > cutoff
            upper_exp = 1 - cdf

            if upper_exp > 1:
                self.logger.warn("Upper exp is larger than 1, "
                                 "is this possible: %g > 1" % upper_exp)
                upper_exp = 1
                cdf = 0

            if upper_exp < tmp_pt or tmp_threshold == tmp_cutoff:
                # There is not enough probability mass higher than cutoff
                # Or threshold == cutoff
                pred[p] = mean_var[0][p]
            else:
                # Calculate mean of lower truncnorm
                lower_pred = stats.truncnorm.stats(
                    a=(-numpy.inf - mean_var[0][p]) / numpy.sqrt(mean_var[1][p]),
                    b=(tmp_cutoff - mean_var[0][p]) / numpy.sqrt(mean_var[1][p]),
                    loc=mean_var[0][p],
                    scale=numpy.sqrt(mean_var[1][p]),
                    moments='m')
                # Add truncnorm mean*cdf + threshold*cdf
                if logged:
                    # We have to use unwarped values as '+' in logspace is not
                    # equal to + in non-logspace
                    upper_pred = upper_exp * numpy.power(10, tmp_threshold)
                    lower_pred = cdf * numpy.power(10, lower_pred)
                    pred[p] = numpy.log10(lower_pred + upper_pred)
                else:
                    upper_pred = upper_exp * tmp_threshold
                    pred[p] = lower_pred * cdf + upper_pred

                if pred[p] > tmp_threshold + 10 ** -5:
                    raise ValueError("Predicted higher than possible, %g > %g"
                                     % (pred[p], tmp_threshold))

                # This can happen and if it happens, set prediction to cutoff
                if not numpy.isfinite(pred[p]):
                    self.logger.critical("Prediction is not finite cdf %g, "
                                         "lower_pred %g; Setting %g to %g" %
                                         (cdf, lower_pred, pred[p],
                                          tmp_cutoff + 10 ** -5))
                    pred[p] = tmp_cutoff + 10 ** -5
        return pred, var