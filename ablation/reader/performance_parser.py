'''
Created on September 15, 2015

@author: Andre Biedenkapp
'''

import csv
import os
import re
import logging

class PerformanceParser(object):
    '''
    reads collected performance data of target algorithm
    '''

    def __init__(self, debug=False):
        '''
        Constructor
        '''
        if debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.ERROR)
        self.logger = logging.getLogger('PParser')
        
    def read_csv_file(self, csv_file, skip_head=True, cap_time=300):
        '''
        Parses a file with the following format:
        |Instance_Name|Status|Seed|Performance|configuration|
        
        :csv_file:  the file to read from
        :skip_head: skip first row in file
        :cap_time:  cutoff time
        '''
        
        assert os.path.isfile(csv_file), '%s is not a file!' % csv_file
        
        instances = list()
        success_list = list()
        censored_list = list()
        performance = list()
        config = list()
        
        with open(csv_file, 'rb') as performanceFile:
            performanceReader = csv.reader(performanceFile, delimiter=',')
            
            if skip_head:
                next(performanceReader)
                
            for row in performanceReader:
                if row == []:
                    continue
                instances.append(row[0].strip(' '))
                
                status = row[2].strip(' ')
                
                if status in ["SAT", "UNSAT", "SUCCESS"]:
                    success_list.append(True)
                    censored_list.append(False)
                elif status == "TIMEOUT":
                    if performance < cap_time:
                        censored_list.append(True)
                    else:
                        censored_list.append(False)
                    success_list.append(False)
                elif status == "CRASHED":
                    success_list.append(False)
                    censored_list.append(False)
                else:
                    raise ValueError("Don't know that state: %s" % status)
                
                performance.append(float(row[3].strip(' ')))
                config.append([i.strip(' ') for i in row[4:]])
        
        return instances, success_list, censored_list, performance, config
    
    def parse_configurations(self, configs, parameter_dict):
        """
        Processes the configurations that are given as Strings and
        extracts the variable names and their values
        
        :configs:           the configurations to parse
        :parameter_dict:    a dictionary with parameter names and the default
                            value of the parameter
        """
        
        NUM_REGEX = re.compile('^.(?P<name>\w+)\=(?P<val>.?[0-9]+\Z)')
        FLOAT_REGEX = re.compile('^.(?P<name>\w+)\=(?P<val>.?[0-9]*\.[0-9]+)')
        WORD_REGEX = re.compile('^.(?P<name>\w+)\=(?P<val>[a-zA-z]+[a-zA-Z\-.]*)')
        
        parsed_configs = dict()
        
        for i in parameter_dict.keys(): # create empty result
            parsed_configs[i] = list()
        
        name = ''
        val = None
        
        # fill the result by going over each and every parameter in a config
        for i in range(len(configs)):
            self.logger.debug('#'*80)
            for j in range(len(configs[i])):
                
                if configs[i][j] == '': # skip empty entries
                    continue
    
                float_match = FLOAT_REGEX.match(configs[i][j])
                if float_match: # is it a float ...
                    self.logger.debug('Found float match: %s = %s'
                                %(float_match.group('name'),
                                float_match.group('val')))
                    name = float_match.group('name')
                    val = float_match.group('val')
                    parameter_type = type(parameter_dict[name])
                    if not isinstance(val, parameter_type): # cast to the type given by
                                                            # the pcs/param_dict
                        val = float(val)
            
                num_match = NUM_REGEX.match(configs[i][j])
                if num_match: # ... an integer or ...
                    self.logger.debug('Found int match: %s = %s'
                                %(num_match.group('name'),
                                num_match.group('val')))
                    name = num_match.group('name')
                    parameter_type = type(parameter_dict[name])
                    val = num_match.group('val')
                    if not isinstance(val, parameter_type): # again cast to the right type
                        val = int(val)
                    
                word_match = WORD_REGEX.match(configs[i][j])
                if word_match: # ... a name (e.g of a heuristic)
                    self.logger.debug('Found non-numerical match: %s = %s'
                                %(word_match.group('name'),
                                word_match.group('val')))
                    name = word_match.group('name')
                    val = word_match.group('val')
                parsed_configs[name].append(val)
                    
            for j in parsed_configs.keys(): # if a parameter was not set in
                if len(parsed_configs[j]) < i + 1: # the config use the default
                    parsed_configs[j].append(parameter_dict[j])
                
                
                
        return parsed_configs
