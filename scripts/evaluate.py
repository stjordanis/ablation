"""
Created on September 16, 2015

@author: Andre Biedenkapp
"""
from __future__ import print_function

import logging
import os
import shutil
import argparse
import time
import glob

from datetime import datetime
from ablation.analysis.ablation_analyser import AblationAnalyser
from pysmac.utils.state_merge import state_merge
from epm.pcs.config_space import ConfigSpace

from compare_validation import parseContents, readData


class AblationArguments:
    def __init__(self, args):
        """
        Constructor
        """
        self.type = args.type
        self.scenario = args.scenario
        self.logy = args.logy
        self.epm_model = args.epm_model
        self.working_dir = None


CENSORED = True
UNCENSORED = False


def map_run_result(res):
    if 'TIMEOUT' in res:
        return CENSORED
    return UNCENSORED


def get_valid_data(file_):

    with open(file_) as valid_f:
        data = list(map(lambda x: x.strip()[1:-1].split('","'), valid_f.readlines()[1:]))

    insts_ = []
    res_stat_ = []
    res_perf_ = []
    for element in data:
        insts_.append(element[0].replace("'", ''))
        tmp = element[2].replace(' ', '').split(',')[:2]
        res_stat_.append(map_run_result(tmp[0]))
        res_perf_.append(float(tmp[1]))

    return insts_, res_stat_, res_perf_


# ---------------------------------------------------------------------------------------------------------------------
def config_dict_to_string(dict_):
    result = ''
    for i in dict_:
        result += '-'+i+" '"+str(dict_[i])+"' "
    return result


def none_or_positive_int(value):
    if value is None:
        return value
    value = int(value)
    if value < 0:
        raise argparse.ArgumentTypeError("%s is an invalid negative int value" % value)
    if value == 0:
        raise argparse.ArgumentTypeError("Invalid sample size")
    return value


# ---------------------------------------------------------------------------------------------------------------------
def main():
    models = ['rf', 'rfr', 'rfrm', 'mrfrm']
    typs = ['epm']  #, 'original']

    parser = argparse.ArgumentParser()
    parser.add_argument('--type', dest='type',
                        choices=typs, default='epm')
    parser.add_argument('--scenario', dest='scenario',
                        required=True, help='Scenario folder')
    parser.add_argument('--dir', dest='working_dir',
                        default='../logs', type=str, help='Where to put the output')
    parser.add_argument('--logy', dest='logy',
                        action="store_true", help='Log y values')
    parser.add_argument('--model', dest='epm_model',
                        required=False, help='Model to use for the epm',
                        default='rfr', choices=models)
    parser.add_argument('--racing', '--race', dest='race',
                        action='store_true', help='Use racing')
    # parser.add_argument('--jarPath', '--jar', dest='jar',
    #                     default='/home/biedenka/git/ablationAnalysis-0.9.2',
    #                     help='For original ablation, the path to the .jar')
    parser.add_argument('--allFrom', '--all', dest='all_runs', default=None, help='root Folder in which all SMAC runs \
                        can be found')
    # parser.add_argument('--maxRounds', dest='maxAblationRounds', default=-1, type=int,
    #                     help='Max # runs for original ablation')
    # parser.add_argument('--onlyOptFile', dest='onlyOptFile', action='store_true',
    #                     help='only generate the optionFile and dont ablate')
    parser.add_argument('--par', dest='PAR',
                        help='par value', required=False, type=float, default=10)
    parser.add_argument('--parI', dest='imputationPAR',
                        help='par value', required=False, type=float, default=None)
    parser.add_argument('--validateEPM', dest='validateEPM',
                        help='Validate EPM Path', action='store_true')
    parser.add_argument('--validateBF', dest='validateBF',
                        help='Path to path of the brute force path',
                        type=str, default=False, required=False)
    parser.add_argument('--skipA', dest='skip_ablation',
                        help='Skip Ablation. Just train the model', action='store_true')
    parser.add_argument('--dontImpute', dest='dontImpute',
                        help='Dont impute EPM in training', action='store_false')
    parser.add_argument('--save', dest='save_model',
                        help='Save EPM as pkl file', action='store_true')
    parser.add_argument('--nsamples', dest='samples',
                        help='Sample Size for imputation. None or positive int',
                        type=none_or_positive_int, default=None)
    parser.add_argument('--notNew', dest='old_Prediction_Method', action='store_false',
                        help='Dont use the new way of calculating the mean and variance.')
    parser.add_argument('--validateOnTrain', dest='validate_on_train', action='store_true',
                        help='Validate the path on the TRAINING set.')
    parser.add_argument('--prevPath', dest='path', action='store_true')
    parser.add_argument('--def-valid-file', dest='def_val', default=None)
    parser.add_argument('--inc-valid-file', dest='inc_val', default=None)
    args, unknown = parser.parse_known_args()

    use = 'TRAINING'

    args.working_dir = os.path.abspath(args.working_dir)
    args.scenario = os.path.abspath(args.scenario)
    if args.all_runs is not None:
        args.all_runs = os.path.abspath(args.all_runs)
    # args.jar = os.path.abspath(args.jar)

    outdir = os.path.sep.join((args.working_dir, 'out'))

    if not os.path.exists(args.working_dir):
        os.mkdir(args.working_dir)
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    os.chdir(args.working_dir)

    scenario_file = open(os.path.sep.join((outdir, 'scenario.txt')), 'w')
    for a in args.__dict__:
        if a in ['dontImpute']:
            scenario_file.write(''.join((a, ' = ', str(not args.__dict__[a]), '\n')))
        else:
            scenario_file.write(''.join((a, ' = ', str(args.__dict__[a]), '\n')))
    scenario_file.close()

    loggedy = ''
    if not args.logy:
        loggedy = 'not'

    log_path = os.path.sep.join((outdir, 'epm_ablation_log.log'))
    previous_log_file = ''

    files_in_outdir = glob.glob(os.path.join(outdir, '*'))
    if files_in_outdir != []:
        new_folder_name = 'prev_files' + time.strftime('%Y_%m_%d_%H_%M_%S', time.gmtime())
        os.mkdir(os.path.join(outdir, new_folder_name))
        for file_ in files_in_outdir:
            if os.path.isfile(file_):
                shutil.copy(file_, os.path.join(outdir, new_folder_name, os.path.split(file_)[-1]))
    if os.path.exists(log_path):
        tmp_file = open(log_path)
        timestr = datetime.fromtimestamp(os.fstat(tmp_file.fileno())[-1])
        tmp_file.close()
        previous_log_file = log_path[:-4] + '_' + str(timestr).replace(' ', '_') + '.log'
        os.rename(log_path, previous_log_file)

    logging.basicConfig(filename=log_path, level=logging.INFO, format=' %(asctime)s %(levelname)s %(message)s')
    ch = logging.StreamHandler()
    formatter = logging.Formatter('%(name)s:%(levelname)s: %(message)s')
    ch.setFormatter(formatter)
    ch.setLevel(logging.DEBUG)
    logger = logging.getLogger('Ablation')
    logger.addHandler(ch)

    logger.info('#'*120)
    logger.info('Data will be loaded from %s' % args.scenario)
    logger.info('Data will be stored in %s' % args.working_dir)
    logger.info('Using %s ablation' % args.type)
    if args.type == 'epm':
        logger.info('Model to use: %s' % args.epm_model)
    logger.info('Data is %s put on logscale' % loggedy)
    logger.info('#'*120)

    pcs = glob.glob(os.path.join(args.scenario, 'state-run*', '*.pcs'))[0]
    cs = ConfigSpace(pcs)

    if args.all_runs is not None:  # Merge all runs in the aclib folder hireachy that contain state-runs
        all_top_folders = glob.glob(os.path.join(args.all_runs, 'run-*'))
        scenario_folder = args.scenario.split(os.path.sep)
        validation_folder = 'validate-*-*'

        for j, i in enumerate(scenario_folder):
            if 'run-' in i:
                sub_folders = os.path.sep.join(scenario_folder[j+1:])
                break

#        all_top_folders = filter(lambda x: x != scenario_folder, all_top_folders)
        all_traj_file_folders = map(lambda x, y: os.path.sep.join((x, y)), all_top_folders,
                                    [sub_folders]*len(all_top_folders))
        all_validation_folders = map(lambda x, y: os.path.sep.join((x, y)), all_top_folders,
                                     [validation_folder]*len(all_top_folders))

        folders = map(lambda x: glob.glob(os.path.join(x, '*state-run*')), all_traj_file_folders)

        traj_files = map(lambda x: glob.glob(os.path.join(x, 'traj-run*')), all_traj_file_folders)
        validation_files = map(lambda x: glob.glob(os.path.join(x, 'validationResults-traj*')),
                               all_validation_folders)

        source = []
        target = []
        linked = False
        for j, i in enumerate(folders):
            if len(i) == len(all_top_folders):
                logger.warning('Already linked all state-runs')
                linked = True
                break
            if len(i) > 1:
                logger.warning('Found %d state-run folders in %s' % (len(i), all_top_folders[j]))
                logger.warning('Skipping')
                continue
            source.append(i[0])
            target.append(os.path.join(args.scenario, i[0].split(os.path.sep)[-1]))

        for i, j in zip(source, target):
            if linked:
                break
            if os.path.sep.join(i.split(os.path.sep)[:-1]) == args.scenario:
                continue
            if not os.path.islink(i):
                os.symlink(i, j)

        best_p = float('inf')
        best_c = {}
        best_i = -1

        for j, i in enumerate(traj_files):
            if len(i) > 1:
                logger.warning('Found %d traj-run files in %s' % (len(i), all_top_folders[j]))
                logger.warning('Using traj-run file %s' % i[0])
                logger.warning('All found files: %s' % str(i))
            traj_files[j] = i[0]
            validation_files[j] = validation_files[j][0]

            perf, config = get_optimized_config(i[0], cs)

            with open(validation_files[j], 'r') as valFile:
                val_data = valFile.read().split('\n')

            # "Time","Training (Empirical) Performance","Test Set Performance"
            val_data = val_data[1].split(',')[:3]

            if use == 'TRAINING':
                perf = float(val_data[1])
            else:
                perf = float(val_data[2])

            if perf < best_p:
                best_p = perf
                best_i = j
                best_c = config

        target = best_c
    else:
        traj_file = os.path.join(args.scenario, 'traj-run-1.txt')
        traj_files = [traj_file]
        best_i = 0
        target = get_optimized_config(traj_file, cs)[1]
    logger.info('Incumbent found in %s' % traj_files[best_i])

    merged_state = os.path.join(
        args.scenario, "merged")

    if not os.path.exists(merged_state):
        state_runs = glob.glob(os.path.join(
            args.scenario, "*state-run*"))  # use old and new data

        state_merge(state_run_directory_list=state_runs,
                    destination=merged_state)

    with open(os.path.abspath(os.path.sep.join((args.scenario, 'merged', 'scenario.txt'))), 'r') as scenarioFile:
        data = scenarioFile.read().split('\n')

    original_algo = ''

    with open('scenario.dat', 'w') as scenarioFile:
        for entry in data:
            tmp = entry.strip('\n')
            tmp = tmp.split(' = ')
            if tmp[0] == 'algo':
                original_algo = tmp[1].replace('./', os.path.sep.join(args.scenario.split(os.path.sep)[:-2]+['']))
                scenarioFile.write(' '.join((tmp[0], tmp[1])))
            elif tmp[0] == 'execdir':
                scenarioFile.write(' '.join((tmp[0], 'out')))
            elif tmp[0] == 'instance_file':
                scenarioFile.write(' '.join((tmp[0], 'instances.txt')))
            elif tmp[0] == 'feature_file':
                scenarioFile.write(' '.join((tmp[0], 'instance-features.txt')))
            elif tmp[0] == 'paramfile':
                scenarioFile.write(' '.join((tmp[0], 'param.pcs')))
            else:
                scenarioFile.write(' '.join(tmp))
            scenarioFile.write('\n')

    link = os.path.sep.join(args.scenario.split(os.path.sep)[:-2]+['target_algorithms'])
    if not os.path.islink('target_algorithms'):
        os.symlink(link, 'target_algorithms')
    link = os.path.sep.join(args.scenario.split(os.path.sep)[:-2]+['instances'])
    if not os.path.islink('instances'):
        os.symlink(link, 'instances')

    if not os.path.islink('instances.txt'):
        os.symlink(args.scenario + os.path.sep + 'merged' + os.path.sep + 'instances.txt', 'instances.txt')
    if not os.path.islink('instance-features.txt'):
        os.symlink(args.scenario + os.path.sep + 'state-run1' + os.path.sep + 'instance-features.txt',
                   'instance-features.txt')
    if not os.path.islink('param.pcs'):
        os.symlink(args.scenario + os.path.sep + 'merged' + os.path.sep + 'param.pcs', 'param.pcs')

    tmp = args.scenario

    args.scenario = 'scenario.dat'

    ablation_args = AblationArguments(args)
    ablation_args.working_dir = tmp

    analyzer = AblationAnalyser(ablation_args)
    analyzer._gather_data()
    args.scenario = tmp

    source = analyzer.get_default_config(pcs)

    count = 0
    for i in source:
        try:
            if target[i] != source[i]:
                count += 1
        except KeyError:
            logger.error('Parameter %s not in target' % i)

    logger.info('Source and target differ in %d parameters' % count)

    count = 0
    for i in target:
        try:
            if target[i] != source[i]:
                count += 1
        except KeyError:
            logger.error('Parameter %s not in source' % i)

    logger.info('Source and target differ in %d parameters' % count)

    if args.def_val:
        # print(list(glob.glob(args.def_val)))
        insts_, cen_, perf_, conf_ = [], [], [], []
        for file_ in glob.glob(args.def_val):
            def_insts_, def_cen_, def_perf_ = get_valid_data(file_)
            def_conf_list = []
            for i in range(len(def_insts_)):
                def_conf_list.append(source)
            insts_.extend(def_insts_)
            cen_.extend(def_cen_)
            perf_.extend(def_perf_)
            conf_.extend(def_conf_list)
        analyzer.def_valid = (conf_, insts_, cen_, perf_)

    if args.inc_val:
        if os.path.exists(args.inc_val):
            inc_insts_, inc_cen_, inc_perf_ = get_valid_data(args.inc_val)
            inc_conf_list = []
            for i in range(len(inc_insts_)):
                inc_conf_list.append(source)
        analyzer.inc_valid = (inc_conf_list, inc_insts_, inc_cen_, inc_perf_)

    if 'epm' == args.type:
        logger.info('Start training EPM')
        if args.save_model:
            save = 'model.pkl'
            save_meta = True
        else:
            save = None
            save_meta = False

        par = args.PAR
        if args.imputationPAR is not None:
            par = args.imputationPAR
        analyzer._train_epm(save=save, save_meta=save_meta, par=par,
                            impute_=args.dontImpute, nsamples=args.samples)
        if args.imputationPAR is not None:
            analyzer.threshold = analyzer.cutoff * args.PAR
        logger.info('Finished training EPM')
        validation_file_ = analyzer.scenario_data['test_instance_file']
        if args.validate_on_train:
            validation_file_ = analyzer.scenario_data['instance_file']

        if not args.skip_ablation:
            logger.info('Start Ablation')
            path = analyzer.ablate_with_epm(target, source, racing=args.race,
                                            closer_look_at_data=True, new=args.old_Prediction_Method)
            path = map(lambda x: x[0], path)[1:]
            logger.info('Finished Ablation')
            logger.info('The resulted path:')
            logger.info(path)

            if args.validateEPM:
                save_file = open(os.path.sep.join((outdir, 'epm_validation.log')), 'w')
                variance_file = open(os.path.sep.join((outdir, 'epm_validation_vars.log')), 'w')
                analyzer.validate_path(path, target, source, validation_file_, save_file,
                                       variance_file, new=args.old_Prediction_Method)
                save_file.close()
                variance_file.close()
        elif args.path and previous_log_file != '':
            next = False
            with open(previous_log_file, 'r') as plf:
                for line in plf.readlines():
                    if 'The resulted path:' in line:
                        print(line)
                        next = True
                    elif next:
                        print(line)
                        path = map(lambda x: [x.replace('[', '').replace(']', '').replace("'", '').strip()],
                                   line.split(' [[')[1].split('], ['))
                print(path)
            if args.validateEPM:
                save_file = open(os.path.sep.join((outdir, 'epm_validation.log')), 'w')
                variance_file = open(os.path.sep.join((outdir, 'epm_validation_vars.log')), 'w')
                analyzer.validate_path(path, target, source, validation_file_, save_file,
                                       variance_file, new=args.old_Prediction_Method)
                save_file.close()
                variance_file.close()

        if args.validateBF:
            bf_dict, bf_path, bf_s, bf_t, bf_r = parseContents(readData(args.validateBF), remove_symbols=False)
            bf_path = map(lambda x: x.split(','), bf_path[1:-1])
            save_file = open(os.path.sep.join((outdir, 'bf_validation.log')), 'w')
            variance_file = open(os.path.sep.join((outdir, 'bf_validation_vars.log')), 'w')
            analyzer.validate_path(bf_path, target, source, validation_file_, save_file,
                                   variance_file, new=args.old_Prediction_Method)
            save_file.close()
            variance_file.close()

    # if args.type == 'original':
    #     target = config_dict_to_string(target)
    #     source = config_dict_to_string(source)
    #     logger.info('Start Ablation')
    #     analyzer.ablate(args.jar, source=source, target=target, algo=original_algo,
    #                     racing=args.race, working_dir=args.working_dir, maxRounds=args.maxAblationRounds, oof=args.onlyOptFile)
    #     logger.info('Finished Ablation')


# -----------------------------------------------------------------------------------------------------------------
def get_optimized_config(traj_file, config_space, as_string=False, prefix='-'):
    """
    Reads the optimized configuration from the given traj_file

    :traj_file: Path to file in which the incumbent is written
    :as_string: Returns the configuration as string instead of a dictionary
    """
    with open(traj_file, 'r') as tF:
        for line in tF:
            pass
    line = line.replace('\n', '').split(', ')
    estimate_performance = float(line[1].strip())
    config = map(lambda x: '-' + x.strip(' ').strip('\n').replace('=', ' '), line[5:])

    if as_string:
        return estimate_performance, '"'+' '.join(config)+'"'
    else:
        optimized_param_dict = {}
        for i in config:
            if i[0] == prefix:
                i = i[1:]
            split_line = i.split(' ')
            param = split_line[0].strip()
            type_ = config_space.parameters[param].type
            value = split_line[1].replace("'", '').strip()
            if type_ == 2:
                value = int(value)
            if type_ == 3:
                value = float(value)
            optimized_param_dict[param] = value

    return estimate_performance, optimized_param_dict


# ---------------------------------------------------------------------
if __name__ == "__main__":
    main()
