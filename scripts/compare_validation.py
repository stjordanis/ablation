from __future__ import print_function

import re
import os
import logging
import argparse
import numpy as np
import matplotlib.pyplot as plt

from sklearn.metrics import mean_squared_error as mse
from scipy import stats

table_header = '\\begin{tabular}{l rr}\n\\toprule\nparameter & %s & %s\\\\'

MAX_SIZE = 19
UP_TIL = 8
FROM = -8


# ---------------------------------------------------------------------------------------------------------------------
def plotPaths(epmParams, epmPerfor, orgParams, orgPerfor, name='', title2='Original',
              save=True, upper=None, lower=None, nparams=None):
    print(nparams)

    if nparams is not None:
        epmParams = epmParams[:nparams]
        epmPerfor = epmPerfor[:nparams]
        orgParams = orgParams[:nparams]
        orgPerfor = orgPerfor[:nparams]
        if upper is not None:
            upper = upper[:nparams]
            lower = lower[:nparams]

    print(len(epmPerfor), len(orgPerfor))
    y = [epmPerfor, orgPerfor]
    x = [range(len(epmParams)), range(len(orgParams))]

    for idx, elem in enumerate(epmParams):  # TODO make paired flipps more obvious
        if len(elem) > MAX_SIZE:
            epmParams[idx] = elem[:UP_TIL] + '...' + elem[FROM:]  # TODO use the first five letters (or so)
    for idx, elem in enumerate(orgParams):
        if len(elem) > MAX_SIZE:
            orgParams[idx] = elem[:UP_TIL] + '...' + elem[FROM:]

    xlabels = [epmParams, orgParams]

    fig, axs = plt.subplots(2, 1, figsize=(10, 10), sharey=False)
    plt.tight_layout(h_pad=11)
    plt.subplots_adjust(bottom=0.2, left=0.125)

    ha = [title2, 'Original']

    for n, ax in enumerate(axs):
        if n == 0 and upper is not None and lower is not None:
            ax.fill_between(x[n], upper, lower, alpha=0.25)
        ax.plot(x[n], y[n])
        ax.set_title(ha[n])
        ax.set_xticks(x[n])
        ax.set_ylabel('performance [sec]')
        ax.set_xticklabels(xlabels[n], rotation=45, ha='right')
        ax.set_xlim([x[n][0], x[n][-1]])

    if save:
        plt.savefig(name)
    else:
        plt.show()


def one_plot(orgPa, oP, epmPa, eP, epmVar=None, nameA='Original', nameB='EPM', name=None, std=True):
    colorA = (0.375, 0.375, 0.375)
    colorB = (0, 0, 0)
    print(len(orgPa), len(epmPa))
    # oP = np.log10(oP)
    # eP = np.log10(eP)

    if std:
        u = map(lambda x, y: x + np.sqrt(y), eP, epmVar)
        l = map(lambda x, y: x - np.sqrt(y), eP, epmVar)
    else:
        u = map(lambda x, y: x + y, eP, epmVar)
        l = map(lambda x, y: x - y, eP, epmVar)

    for idx, elem in enumerate(epmPa):
        if len(elem) > MAX_SIZE:
            epmPa[idx] = elem[:UP_TIL] + '...' + elem[FROM:]
    for idx, elem in enumerate(orgPa):
        if len(elem) > MAX_SIZE:
            orgPa[idx] = elem[:UP_TIL] + '...' + elem[FROM:]

    fig = plt.figure(figsize=(20, 10))
    ax1 = fig.add_subplot(111)
    ax2 = ax1.twiny()
    plt.subplots_adjust(bottom=0.25, top=0.75, left=0.05, right=.95)

    if epmVar is not None:
        ax1.fill_between(range(len(oP)), u, l, alpha=0.2, color=colorB)
    ax1.plot(range(len(eP)), eP, label=nameB, color=colorB)
    ax1.plot(range(len(oP)), oP, label=nameA, color=colorA)

    ax1.set_xlabel(nameA + ' Path', color=colorA)
    ax1.set_xticks(range(len(orgPa)))
    ax2.set_xlabel(nameB + ' Path', color=(0, 0, 0))
    ax2.set_xticks(range(len(epmPa)))

    ax1.set_xticklabels(orgPa, rotation=45, ha='right', color=colorA)
    ax2.set_xticklabels(epmPa, rotation=45, ha='left', color=colorB)

    ax1.set_xlim(0, len(orgPa) - 1)
    ax2.set_xlim(0, len(epmPa) - 1)

    ax1.legend()
    ax1.set_ylabel('runtime [sec]')
    max_ = max(max(max(oP), max(eP)), max(u))
    min_ = min(min(min(oP), min(eP)), min(l))
    if min_ <= 0:
        min_ += 0.1*min_
    else:
        min_ -= 0.1*min_
    if max_ <= 0:
        max_ -= 0.1*max_
    else:
        max_ += 0.1*max_
    ax1.set_ylim(min_, max_)

    print(name)

    if name is not None:
        plt.savefig(name)

# ---------------------------------------------------------------------------------------------------------------------
def diff_param_list(a, b):
    a = sorted(a)
    b = sorted(b)
    set_a = set(a)
    set_b = set(b)
    
    not_in_a = []
    not_in_b = []
    
    for i in set_a:
        if i not in set_b:
            not_in_b.append(i)
    for i in set_b:
        if i not in set_a:
            not_in_a.append(i)
    return not_in_a, not_in_b


# ---------------------------------------------------------------------------------------------------------------------
def main():
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()

    parser = argparse.ArgumentParser()
    parser.add_argument('validFile_one')  # org
    parser.add_argument('validFile_two')  # epm
    parser.add_argument('--plot', dest='name', help='png file name', default='plot.png')
    parser.add_argument('--race', dest='race', action='store_true')
    parser.add_argument('--table', dest='file_', default='table.tex')
    parser.add_argument('--nargs', dest='nargs', default=-1, type=int)
    parser.add_argument('--var', dest='var', default=None)
    args, unknown = parser.parse_known_args()

    validation_one = readData(args.validFile_one)
    validation_two = readData(args.validFile_two)
    if args.var is not None:
        var_data_two = readData(args.var)
    else:
        var_data_two = None

    var_one_dict, var_one_list, source_val_one, target_val_one, result_one = parseContents(validation_one)  # original

    var_two_dict, var_two_list, source_val_two, target_val_two, result_two = parseContents(validation_two)  # epm

    if var_data_two is not None:
        var_var_two_dict, var_var_two_list, var_source_val_two, var_target_val_two, var_result_two = parseContents(
            var_data_two)  # epm
    else:
        var_var_two_dict, var_var_two_list, var_source_val_two, var_target_val_two, var_result_two = var_two_dict, \
                                                                                                     var_two_list, \
                                                                                                     source_val_two, \
                                                                                                     target_val_two, \
                                                                                                     result_two

    # find out if and which parameter is not in both paths
    # This indicates a deacitvated parameter with 0 importance
    not_in_one, not_in_two = diff_param_list(var_one_list, var_two_list)
    
    if not_in_one != []:
        var_one_list = var_one_list[:-1]
        tmp = result_one[-1]
        result_one = result_one[:-1]
        dict_tmp = var_one_dict['-target-']
        for i in not_in_one:
            var_one_list.append(i)
            result_one.append(result_one[-1])
            var_one_dict[i] = dict_tmp
            dict_tmp += 1
        var_one_dict['-target-'] = dict_tmp
        result_one.append(tmp)
        var_one_list.append('-target-')
        
    
    if not_in_two != []:
        var_two_list = var_two_list[:-1]
        var_var_two_list = var_var_two_list[:-1]

        tmp = result_two[-1]
        tmp2 = var_result_two[-1]

        result_two = result_two[:-1]
        var_result_two = var_result_two[:-1]

        dict_tmp = var_two_dict['-target-']
        var_dict_tmp = var_var_two_dict['-target-']

        for i in not_in_two:
            var_two_list.append(i)
            var_var_two_list.append(i)

            result_two.append(result_two[-1])
            var_result_two.append(var_result_two[-1])

            var_two_dict[i] = dict_tmp
            var_var_two_dict[i] = var_dict_tmp

            dict_tmp += 1
            var_dict_tmp += 1

        var_two_dict['-target-'] = dict_tmp
        var_var_two_dict['-target-'] = var_dict_tmp

        result_two.append(tmp)
        var_result_two.append(tmp2)

        var_two_list.append('-target-')
        var_var_two_list.append('-target-')

    per_two = percentage(result_two)
    per_one = percentage(result_one)

    if var_data_two is not None:
        per_two_l = np.array(result_two) - np.sqrt(var_result_two)
        per_two_u = np.array(result_two) + np.sqrt(var_result_two)
    else:
        per_two_u, per_two_l = None, None

    pos = [0 for i in range(len(var_one_dict))]
    # _pos = [0 for i in range(len(per_one))]
    for k in var_one_dict:
        pos[var_one_dict[k] + 1] = k

    tmp = map(lambda x: (var_one_dict[x], var_two_dict[x]), pos)
    print(stats.spearmanr(tmp))

    if args.nargs > len(var_one_dict) - 2:
        args.nargs = len(var_one_dict) - 2

    print(len(tmp[1:args.nargs + 1]))
    if args.nargs != -1:
        spearman = stats.spearmanr(tmp[1:args.nargs + 1])[0]
    else:
        spearman = stats.spearmanr(tmp[1:-1])[0]

    var_ = var_result_two if var_data_two is not None else None

    nB = 'Racing' if args.race else 'EPM'

    printTable(var_one_list, var_two_list, var_two_dict, per_one, per_two, args, args.file_, nparams=args.nargs,
               spear=spearman)
    one_plot(var_one_list, result_one, var_two_list, result_two, var_, name=args.name, nameB=nB)

    # if args.race:
    #     col_name = 'Racing'
    # else:
    #     col_name = 'EPM'

    # plotPaths(var_two_list, result_two, var_one_list, result_one, name=args.name,
    #           title2=col_name, upper=per_two_u, lower=per_two_l, nparams=args.nargs)


def printTable(var_one_list, var_two_list, var_two_dict, per_one, per_two, args, file_=None, sort=False, nparams=-1,
               spear=None):
    """
    Orginal Ablation Path        | %PAR10 Change            || %PAR10 Change            | EPM Ablation Path
    -----------------------------------------------------------------------------------------------------------
    param1                       | 80%                      || 75%                      | param1
    param2                       | 10%                      ||  8%                      | param3
    param3                       |  5%                      ||  6%                      | param2
    """

    rmse_order = []
    for i in var_one_list:
        if i[0] == '-':
            continue
        rmse_order.append(var_two_dict[i])
    if sort:
        two_order = rmse_order
    else:
        two_order = [i - 1 for i in range(len(var_two_list))][1:-1]

    if args.race:
        col_name = 'Racing'
    else:
        col_name = 'EPM'

    outstring = ''

    rmse = np.sqrt(mse(per_one, np.array(per_two)[rmse_order]))
    outstring += '\\begin{tabular}{rr rl}\n\\toprule\n'
    outstring += 'Orginal Ablation Path & ' + '\\multicolumn{2}{c}{\%PAR10 Change}' +\
                 ' & {:s} Ablation Path\\\\\n'.format(col_name)
    outstring += '\\hline\n'
    count = 0
    print_stuff = True
    n_params_at_same_pos = 0
    if nparams >= 0:
        count += (len(var_two_list) - 2) - nparams
    for i, j, k, l in zip(var_one_list[1:-1], per_one, np.array(per_two)[two_order],
                          np.array(var_two_list[1:-1])[two_order]):
        if len(i) > MAX_SIZE and ',' not in i:
            i = i[:UP_TIL] + '...' + i[FROM:]
        if len(l) > MAX_SIZE and ',' not in l:
            l = l[:UP_TIL] + '...' + l[FROM:]
        i = i.replace('_', '\\_')
        l = l.replace('_', '\\_')
        if i == l:
            i = '\\textbf{' + i + '}'
            l = '\\textbf{' + l + '}'
            n_params_at_same_pos += 1
        if print_stuff:
            outstring += '%s & %3.2f & %3.2f & %s\\\\\n' % (i, j, k, l)
            count += 1
            if count < len(var_two_list) - 2:
                outstring += '\\midrule\n'
            else:
                print_stuff = False
    outstring += '\\bottomrule\n'
    outstring += '\\multicolumn{4}{c}{RMSE: %3.5f}\\\\\n' % rmse
    if spear is not None:
        outstring += '\\multicolumn{4}{c}{Spearman: %3.2f}\n' % spear
    outstring += '\\end{tabular}\n'
    # print sum(per_one), sum(per_two)

    if file_ is None:
        print(outstring)
    else:
        with open(file_, 'w') as outf:
            outf.write(outstring)

# ---------------------------------------------------------------------------------------------------------------------
def readData(path):
    path = os.path.abspath(path)
    assert os.path.exists(path), '%s' % path

    with open(path, 'r') as vF:
        validation_data = vF.read()
    validation_data = re.sub('  +', ' ', validation_data)
    return validation_data.split('\n')[:-1]


# ---------------------------------------------------------------------------------------------------------------------
def percentage(result):
    delta = float(result[0] - result[-1])
    at = 1
    percentages = []
    for i in result[1:-1]:
        percentages.append(((i - result[at-1]) / delta)*100.)
        at += 1
    return percentages


# ---------------------------------------------------------------------------------------------------------------------
def parseContents(data, remove_symbols=True):
    head_count = 0
    variable_dict = {}
    variable_list = []
    source = []
    target = []
    result = []
    for line in data:
        line = line.strip()
        if line.startswith('--'):
            head_count += 1
        elif head_count == 2:
            line = line.replace(', ', ',')
            tmp = line.split(' ')
            if ',' in tmp[1]:
                tmp[1] = ','.join(sorted(tmp[1].split(',')))
            if ':' in tmp[1] and remove_symbols:
                tmp[1] = tmp[1].split(':')
                if len(tmp[1]) in [2, 3]:
                    tmp[1] = '_'.join(tmp[1][1:])
                elif len(tmp[1]) > 3:
                    tmp[1] = '_'.join(tmp[1][2:])
            variable_dict[tmp[1]] = int(tmp[0]) - 1
            variable_list.append(tmp[1])
            source.append(tmp[2])
            target.append(tmp[3])
            result.append(float(tmp[4]))
    return variable_dict, variable_list, source, target, result


if __name__ == '__main__':
    main()
